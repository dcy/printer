var LodopPrinter;

function needCLodop() {
    return true; //本例子强制所有浏览器都调用C-Lodop
};

if (needCLodop()) {
    //====页面动态加载C-Lodop云打印必须的文件CLodopfuncs.js====

    var head = document.head || document.getElementsByTagName("head")[0] || document.documentElement;

    //让其它电脑的浏览器通过本机打印（仅适用C-Lodop自带的例子）：
    var oscript = document.createElement("script");
    oscript.src = "/CLodopfuncs.js";
    head.insertBefore(oscript, head.firstChild);

    //让本机的浏览器打印(更优先一点)：
    oscript = document.createElement("script");
    oscript.src = "http://localhost:8001/CLodopfuncs.js?priority=2";
    head.insertBefore(oscript, head.firstChild);

    //加载双端口(8000和18000）避免其中某个端口被占用：
    oscript = document.createElement("script");
    oscript.src = "http://localhost:18000/CLodopfuncs.js?priority=1";
    head.insertBefore(oscript, head.firstChild);
}



//====获取LODOP对象的主过程：====
function getLodop(oOBJECT, oEMBED) {
    var LODOP;
    try {
        try {
            LODOP = getCLodop();
        } catch (err) {};
        if (!LODOP && document.readyState !== "complete") {

            alert("C-Lodop没准备好，请稍后再试！");
            return;
        };

        if (LODOP.webskt && LODOP.webskt.readyState == 1) {} else {
            alert("C-Lodop没准备好，请稍后再试！");
            return;
        }
        //清理原例子内的object或embed元素，避免乱提示：
        if (oEMBED && oEMBED.parentNode) oEMBED.parentNode.removeChild(oEMBED);
        if (oOBJECT && oOBJECT.parentNode) oOBJECT.parentNode.removeChild(oOBJECT);
        return LODOP;

    } catch (err) {
        alert("getLodop出错:" + err);
     };
};

//printTemplate：打印模板，printModel：需要打印的对象，tableField：重复打印项
function WdbPrint(printTemplate, printModel, tableField) {

    if (LodopPrinter == null && LodopPrinter == undefined) {
        LodopPrinter = getLodop(document.getElementById('LODOP_OB'), document.getElementById('LODOP_EM'));
    }
    if (LodopPrinter == null) {
        return;
    }
    if (printTemplate != null && printTemplate.content != '' && printTemplate.content != null && printModel != null) {

        LodopPrinter.PRINT_INITA(parseInt(printTemplate.move_top) + 'mm',parseInt(printTemplate.move_left) + 'mm',parseInt(printTemplate.page_width) + 'mm', parseInt(printTemplate.page_height) + 'mm',printTemplate.temp_name)//初始化
        // 设置打印纸张尺寸
        LodopPrinter.SET_PRINT_PAGESIZE(printTemplate.direction, parseInt(printTemplate.page_width)+ 'mm', parseInt(printTemplate.page_height) + 'mm', "CreatCustomPage");
        // LodopPrinter.SET_SHOW_MODE('HIDE_PAPER_BORDER', 1);
        // LodopPrinter.SET_SHOW_MODE('NP_NO_RESULT', true);
        LodopPrinter.SET_PRINT_MODE('CUSTOM_TASK_NAME', printTemplate.temp_name);//设置本次输出的打印任务名(打印任务池里的“文档名”)。
        LodopPrinter.SET_PRINT_MODE('AUTO_CLOSE_PREWINDOW', true);//设置打印完毕是否自动关闭预览窗口。
        LodopPrinter.SET_PRINT_MODE("FULL_WIDTH_FOR_OVERFLOW", true);//设置宽度方向上的内容溢出自动缩小。
        LodopPrinter.SET_PRINT_MODE("FULL_HEIGHT_FOR_OVERFLOW", true);//设置高度方向上的内容溢出自动缩小。
        //LodopPrinter.SET_PRINT_MODE("TEXT_ONLY_MODE", true);//设置是否用“纯文本行模式”(后面有说明)快速打印.默认值为“否”，这种模式仅适合部分老款的非图形打印机，只打印文本部分，速度快，图形内容被忽略，请慎用。。
        LodopPrinter.SET_PRINT_MODE("POS_BASEON_PAPER", true);// 为使内容不出轨，输出位置一般是以“可打区域”边缘为基点的，但由于各种打印机物理边距不一样，这样就造成同一程序用不同打印机输出的位置有差异，这对通用套打是非常不利的，此时可用如下语句：
        LodopPrinter.SET_PRINT_MODE("SEND_RAW_DATA_ENCODE", 'UTF-8');//设置SEND_PRINT_RAWDATA语句的数据编码集。UTF-8 UTF-7 UNICODE ANSI UTF-16 UTF-16BE GBK BIG5 EUC-JP
       
     
        //设置打印机名称
        if (printTemplate.printer_name != null && printTemplate.printer_name != '' && printTemplate.printer_name != ' ') {
            LodopPrinter.SET_PRINTER_INDEX(printTemplate.printer_name);
        }

        if (printModel.length == undefined) //PrintModel is object
        {
            DrawPrintPage(printTemplate, printModel, tableField);
        } else if (printModel.length == 1) //PrintModel is list
        {
            DrawPrintPage(printTemplate, printModel[0], tableField);
        } else if (printModel.length > 1) //PrintModel is list
        {
            for (var i = 0; i < printModel.length; i++) {
                DrawPrintPage(printTemplate, printModel[i], tableField);
                LodopPrinter.NewPage();
            }
        }
    }


}


function DrawPrintPage(printTemplate, printModel, tableField) {
    
    var printContent = eval('(' + printTemplate.content + ')'),
        printSingleList = printContent.ItemList,
        printTableList = printContent.ItemDetailList;

    if (printTemplate.background != null && printTemplate.background != '') {
        LodopPrinter.ADD_PRINT_SETUP_BKIMG("<img  border='0'  src='" + printTemplate.background + "'>");
        LodopPrinter.SET_SHOW_MODE("BKIMG_LEFT", 0);
        LodopPrinter.SET_SHOW_MODE("BKIMG_TOP", 0);
        if(printTemplate.bkimg_width)
            LodopPrinter.SET_SHOW_MODE("BKIMG_WIDTH", printTemplate.bkimg_width +'mm');
        if(printTemplate.bkimg_height)
            LodopPrinter.SET_SHOW_MODE("BKIMG_HEIGHT", printTemplate.bkimg_height +'mm');

        LodopPrinter.SET_SHOW_MODE("BKIMG_PRINT", printTemplate.bkimg_flg == 'on' ? true : false);//打印和预览时背景图是否显示
        //LodopPrinter.SET_SHOW_MODE("BKIMG_IN_PREVIEW", printTemplate.bkimg_flg == 'on' ? true : false); //预览时背景图是否显示
        
    } 
    if (printSingleList != null) {
        for (var singleKey in printSingleList) {
            var item = printSingleList[singleKey],
                fontFamily = item.FontFamily == "undefined" ? "微软雅黑" : item.FontFamily,
                prefix = '<div style="text-align:' + item.Alignment + ';font-size:' + item.FontSize + 'px;font-family:' + fontFamily + ';font-weight:' + item.Weight + '">',
                subfix = '</div>';

            if (item.ItemType == "HLine") {
                LodopPrinter.ADD_PRINT_LINE(item.Top, item.Left, item.Top + 1, item.Left + item.Width, 0, 1);
            } else if (item.ItemType == "VLine") {
                LodopPrinter.ADD_PRINT_LINE(item.Top, item.Left, item.Top + item.Height, item.Left + 1, 0, 1);
            }else if (item.ItemType == "SelfText") {
                var v = prefix + item.ItemName + subfix
                LodopPrinter.ADD_PRINT_HTM(item.Top, item.Left, item.Width, item.Height, v);
            } else if (item.ItemType == "Rect") {
                LodopPrinter.ADD_PRINT_RECT(item.Top, item.Left, item.Width, item.Height, 0, 1);
            } else if (item.ItemType == "CustomImage") {
                LodopPrinter.ADD_PRINT_IMAGE(item.Top, item.Left, item.Width, item.Height, "<img border='0' src='" + item.ItemName + "' width='" + item.Width + "px' height='" + item.Height + "'/>");
            }else if (item.ItemType == "PrintTime") {
                var v = prefix + (new Date().toLocaleString()) + subfix;
                LodopPrinter.ADD_PRINT_HTM(item.Top, item.Left, item.Width, item.Height, v);
            }else {
                var v = printModel[item.Code];
                if (v == '' || v == null || v == undefined) continue;
                if (item.ItemType == "QRCode" || item.ItemType == "Code39" || item.ItemType == "Code93" || item.ItemType == "128Auto") {
                    LodopPrinter.ADD_PRINT_BARCODE(item.Top, item.Left, item.Width, item.Height, item.ItemType, v);
                    LodopPrinter.SET_PRINT_STYLEA(0, "ShowBarText", 0);
                    LodopPrinter.SET_PRINT_STYLEA(0, "NotOnlyHighPrecision", true);
               
                }else if(item.ItemType == 1){

                    var html = prefix + v + subfix;
                    LodopPrinter.ADD_PRINT_HTM(item.Top, item.Left, item.Width, item.Height, html);
                }
            }
           
        }

    }
 
    if(printTableList != null)
    {
        var pageIndex = 0;//定义table页条数
        var totalWidth = parseInt(printTemplate.page_width)  * 3.78,
            totalHeight = parseInt(printTemplate.page_height)  * 3.78,
            left = 0,
            top = 0,
            height = 20,
            y = 0;
            for (var tableKey in printTableList)
            {
                var item = printTableList[tableKey];
                if (y == 0) {
                    top = item.Top;
                    left = item.Left;
                    height = item.Height;
                    totalHeight = parseInt(printTemplate.page_height)  * 3.78 - parseInt(top)
                    pageIndex =  Math.floor((parseInt(totalHeight) -30 - Math.floor(parseFloat(printTemplate.move_top)  * 3.78 * 2)) / (parseInt(height) + 1)) 
                }
                y++;
              
            }

        var entityList = printModel[tableField];
        if (entityList != null && entityList.length > 0) {
            var pageNumber = 0;//计算打印页数
            for (var i = 0; i < entityList.length; i++) {

                if (i % pageIndex == 0) {
                    pageNumber += 1
                 }
            }


            for (var j = 0; j < pageNumber; j++) {
            	if(j == 0 && pageNumber > 1)
            		LodopPrinter.ADD_PRINT_TEXT(15, totalWidth - 150, 100, 24, "第" + (j + 1) + "页/共" + pageNumber + "页");

                if(j >0){

                    LodopPrinter.NewPage();
                    LodopPrinter.ADD_PRINT_TEXT(15, totalWidth - 150, 100, 24, "第" + (j + 1) + "页/共" + pageNumber + "页");
                    if (printSingleList != null) {
                        for (var singleKey in printSingleList) {
                            var item = printSingleList[singleKey],
                                fontFamily = item.FontFamily == "undefined" ? "微软雅黑" : item.FontFamily,
                                prefix = '<div style="text-align:' + item.Alignment + ';font-size:' + item.FontSize + 'px;font-family:' + fontFamily + ';font-weight:' + item.Weight + '">',
                                subfix = '</div>';

                            if (item.ItemType == "HLine") {
                                LodopPrinter.ADD_PRINT_LINE(item.Top, item.Left, item.Top + 1, item.Left + item.Width, 0, 1);
                            } else if (item.ItemType == "VLine") {
                                LodopPrinter.ADD_PRINT_LINE(item.Top, item.Left, item.Top + item.Height, item.Left + 1, 0, 1);
                            }else if (item.ItemType == "SelfText") {
                                var v = prefix + item.ItemName + subfix
                                LodopPrinter.ADD_PRINT_HTM(item.Top, item.Left, item.Width, item.Height, v);
                            } else if (item.ItemType == "Rect") {
                                LodopPrinter.ADD_PRINT_RECT(item.Top, item.Left, item.Width, item.Height, 0, 1);
                            } else if (item.ItemType == "CustomImage") {
                                LodopPrinter.ADD_PRINT_IMAGE(item.Top, item.Left, item.Width, item.Height, "<img border='0' src='" + item.ItemName + "' width='" + item.Width + "px' height='" + item.Height + "'/>");
                            }else if (item.ItemType == "PrintTime") {
                                var v = prefix + (new Date().toLocaleString()) + subfix;
                                LodopPrinter.ADD_PRINT_HTM(item.Top, item.Left, item.Width, item.Height, v);
                            }else {
                                var v = printModel[item.Code];
                                if (v == '' || v == null || v == undefined) continue;
                                if (item.ItemType == "QRCode" || item.ItemType == "Code39" || item.ItemType == "Code93" || item.ItemType == "128Auto") {
                                    LodopPrinter.ADD_PRINT_BARCODE(item.Top, item.Left, item.Width, item.Height, item.ItemType, v);
                                    LodopPrinter.SET_PRINT_STYLEA(0, "ShowBarText", 0);
                                    LodopPrinter.SET_PRINT_STYLEA(0, "NotOnlyHighPrecision", true);
                               
                                }else{

                                    var html = prefix + v + subfix;
                                    LodopPrinter.ADD_PRINT_HTM(item.Top, item.Left, item.Width, item.Height, html);
                                }
                            }
                           
                        }

                    }
                }
                
                var tableHtml = '';
                tableHtml += "<table border='" + (printTemplate.table_has_border == 'on' ? 1 : 0) + "' bordercolor='#000000' cellspacing='0' cellpadding='0' style='border-collapse:collapse'>";
                

                if (printTemplate.table_has_header == 'on')
                {
                    
                    tableHtml += "<thead>";
                    tableHtml += "<tr>";
                }
                for (var tableKey in printTableList)
                {
                    var item = printTableList[tableKey];
                    
                    if (printTemplate.table_has_header == 'on') {
                        if (item.Code != null && item.Code != '')
                            tableHtml += "<td  style='width:"+item.Width+"px;height:"+height+"px;font-size:"+ item.FontSize +"px;font-weight:bold;text-align:"+item.Alignment+";font-family:\"" +(item.FontFamily == "undefined" ? "微软雅黑" : item.FontFamily)+ "\"'>" + item.ItemName + "</td>";
                    }
                }
                if (printTemplate.table_has_header == 'on') {
                    tableHtml += "</tr>";
                    tableHtml += "</thead>";
                }
                tableHtml += "<tbody>";
                for (var i = 0; i < entityList.length; i++) {
                    if (i >= j * pageIndex && i < j * pageIndex + pageIndex) {
                        tableHtml += "<tr>";
                        for(var tableKey in printTableList) {
                            var item = printTableList[tableKey];
                            if (item.Code != null && item.Code != '') {
                                if (item.Code == "RowNumber")
                                    tableHtml += "<td style='width:"+item.Width+"px;height:"+height+"px;font-size:" + item.FontSize + "px;font-family:\"" +(item.FontFamily == "undefined" ? "微软雅黑" : item.FontFamily)+ "\";text-align:" + item.Alignment + ";' >" + (i + 1) + "</td>";
                                else {
                                    var v = entityList[i][item.Code];
                                    if (v != null && v != '' && v != undefined) {
                                        tableHtml += "<td style=\"width:"+item.Width+"px;;height:"+height+"px;font-size:" + item.FontSize + "px;font-family:'" + (item.FontFamily == "undefined" ? "微软雅黑" : item.FontFamily) + "';text-align:" + item.Alignment + "\";>" + v + "</td>";
                                    } else {
                                        tableHtml += "<td style=\"width:"+item.Width+"px;;height:"+height+"px;font-size:" + item.FontSize + "px;font-family:'" + (item.FontFamily == "undefined" ? "微软雅黑" : item.FontFamily) + "';text-align:" + item.Alignment + "\";></td>";
                                    }
                                }
                            } 
                        }
                        tableHtml += "</tr>";
                    }
                  
                }
                tableHtml += "<tbody>";

                tableHtml += "</table>";
                LodopPrinter.ADD_PRINT_TABLE(top, left, totalWidth, totalHeight,  tableHtml);
                
            }
                
        }

    }


          

   
}
var WdbPrinter = {
    Print: function(printTemplate, printModel, tableField, isPreview) {

        try {
            var isPrint = false;
            WdbPrint(printTemplate, printModel, tableField);
            if (isPreview)
                return LodopPrinter.PREVIEW() == 1;
            else
                return LodopPrinter.PRINT();
        } catch (e) {
            //console.log(e.message)
            //alert('没有安装打印控件。');
            return false;
        }
    }


}