﻿# Host: localhost  (Version: 5.5.40)
# Date: 2017-12-27 15:07:29
# Generator: MySQL-Front 5.3  (Build 4.120)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "print_fields"
#

DROP TABLE IF EXISTS `print_fields`;
CREATE TABLE `print_fields` (
  `fieldid` bigint(18) NOT NULL AUTO_INCREMENT,
  `print_type` int(10) DEFAULT '0' COMMENT '打印类型',
  `field_name` varchar(50) DEFAULT '' COMMENT '标识',
  `parent_id` bigint(18) DEFAULT '0' COMMENT '父级元素',
  `field_title` varchar(50) DEFAULT '' COMMENT '标题',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`fieldid`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

#
# Data for table "print_fields"
#

INSERT INTO `print_fields` VALUES (240,1,'order',0,'订单',0),(241,2,'goods',0,'商品',0),(242,1,'order_no',240,'单号',0),(243,2,'goods_name',241,'货品名称',0),(244,1,'order_price',240,'订单金额',0),(245,2,'goods_weight',241,'重量',0),(246,2,'goods_price',241,'售价',0);

#
# Structure for table "print_mod_field"
#

DROP TABLE IF EXISTS `print_mod_field`;
CREATE TABLE `print_mod_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Data for table "print_mod_field"
#

/*!40000 ALTER TABLE `print_mod_field` DISABLE KEYS */;
INSERT INTO `print_mod_field` VALUES (1,241,1),(2,240,1);
/*!40000 ALTER TABLE `print_mod_field` ENABLE KEYS */;

#
# Structure for table "print_module"
#

DROP TABLE IF EXISTS `print_module`;
CREATE TABLE `print_module` (
  `moduleid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`moduleid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#
# Data for table "print_module"
#

INSERT INTO `print_module` VALUES (1,'质量保证单'),(2,'原料库存明细单'),(3,'发货、退换货'),(4,'原材料入库单'),(5,'首饰入库单'),(7,'原材料出库单'),(8,'首饰信息单'),(9,'维修单');

#
# Structure for table "print_template"
#

DROP TABLE IF EXISTS `print_template`;
CREATE TABLE `print_template` (
  `id` bigint(18) NOT NULL AUTO_INCREMENT,
  `temp_name` varchar(50) DEFAULT '' COMMENT '模板名称',
  `temp_type` int(10) DEFAULT '0' COMMENT '模板类型(数字)',
  `type_name` varchar(30) DEFAULT '' COMMENT '类型名称',
  `page_width` int(10) DEFAULT '0' COMMENT '纸张宽度',
  `page_height` int(10) DEFAULT '0' COMMENT '纸张高度',
  `direction` int(10) DEFAULT '0' COMMENT '打印方向(0:打印机缺省设置,1:纵(正)向打印,2:横向打印,3:纵向，高度自适应)',
  `printer_name` varchar(50) DEFAULT '' COMMENT '打印机名称',
  `default_font_family` varchar(20) DEFAULT '',
  `default_font_size` int(10) DEFAULT '0',
  `text_align` varchar(10) DEFAULT '',
  `bkimg_width` int(10) DEFAULT '0' COMMENT '背景宽度',
  `bkimg_height` int(10) DEFAULT '0' COMMENT '背景高度',
  `background` varchar(300) DEFAULT '' COMMENT '背景',
  `bkimg_flg` varchar(3) DEFAULT '' COMMENT '是否打印背景on:打印',
  `move_top` int(11) DEFAULT '0' COMMENT '向下偏移',
  `move_left` int(11) DEFAULT '0' COMMENT '向右偏移',
  `content` varchar(21000) DEFAULT NULL COMMENT '内容',
  `table_has_border` varchar(3) DEFAULT '' COMMENT '表格是否打印边框',
  `table_has_header` varchar(3) DEFAULT '' COMMENT '表格是否要打印表头',
  `font_weight` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

#
# Data for table "print_template"
#

INSERT INTO `print_template` VALUES (48,'淘宝订单',1,'质量保证单',200,170,0,'Microsoft XPS Document Writer','微软雅黑',14,'center',200,170,'http://127.0.0.1:8000/printer/public/uploads/20171227/529c943412ce06923480b753f5c6bfee.JPG','on',0,0,'{\"ItemList\":[{\"ItemId\":\"order-order_no-22448-77\",\"ItemName\":\"[$单号]\",\"Code\":\"order_no\",\"ItemType\":\"1\",\"Width\":181,\"Height\":21,\"Left\":85,\"Top\":99,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"left\"},{\"ItemId\":\"order-order_price-58242-3360\",\"ItemName\":\"[$订单金额]\",\"Code\":\"order_price\",\"ItemType\":\"1\",\"Width\":130,\"Height\":21,\"Left\":339,\"Top\":100,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"left\"},{\"ItemId\":\"SelfText-9838\",\"ItemName\":\"淘宝订单\",\"Code\":\"SelfText\",\"ItemType\":\"SelfText\",\"Width\":100,\"Height\":27,\"Left\":300,\"Top\":19,\"FontSize\":22,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"right\"},{\"ItemId\":\"SelfText-3235\",\"ItemName\":\"订单号\",\"Code\":\"SelfText\",\"ItemType\":\"SelfText\",\"Width\":70,\"Height\":19,\"Left\":12,\"Top\":98,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"right\"},{\"ItemId\":\"SelfText-4080\",\"ItemName\":\"订单金额\",\"Code\":\"SelfText\",\"ItemType\":\"SelfText\",\"Width\":70,\"Height\":20,\"Left\":266,\"Top\":101,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"right\"},{\"ItemId\":\"HLine-1326\",\"ItemName\":\"\",\"Code\":\"HLine\",\"ItemType\":\"HLine\",\"Width\":742,\"Height\":6,\"Left\":6,\"Top\":140,\"FontSize\":12,\"Weight\":400,\"FontFamily\":\"\'Helvetica Neue\', Helvetica, Arial, sans-serif\",\"Alignment\":\"start\"},{\"ItemId\":\"VLine-3375\",\"ItemName\":\"\",\"Code\":\"VLine\",\"ItemType\":\"VLine\",\"Width\":6,\"Height\":346,\"Left\":446,\"Top\":250,\"FontSize\":12,\"Weight\":400,\"FontFamily\":\"\'Helvetica Neue\', Helvetica, Arial, sans-serif\",\"Alignment\":\"start\"},{\"ItemId\":\"Rect-3747\",\"ItemName\":\"\",\"Code\":\"Rect\",\"ItemType\":\"Rect\",\"Width\":218,\"Height\":98,\"Left\":208,\"Top\":344,\"FontSize\":10,\"Weight\":400,\"FontFamily\":\"宋体\",\"Alignment\":\"start\"},{\"ItemId\":\"SelfText-7487\",\"ItemName\":\"打印时间\",\"Code\":\"SelfText\",\"ItemType\":\"SelfText\",\"Width\":69,\"Height\":23,\"Left\":472,\"Top\":99,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"left\"},{\"ItemId\":\"PrintTime-8820\",\"ItemName\":\"$[打印时间]\",\"Code\":\"PrintTime\",\"ItemType\":\"PrintTime\",\"Width\":195,\"Height\":23,\"Left\":542,\"Top\":100,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"left\"},{\"ItemId\":\"CustomImage-4180\",\"ItemName\":\"http://127.0.0.1:8000/printer/public/uploads/20171227/9615d9a229fccb8152c5a08c57eb8345.JPG\",\"Code\":\"CustomImage\",\"ItemType\":\"CustomImage\",\"Width\":148,\"Height\":148,\"Left\":58,\"Top\":239,\"FontSize\":12,\"Weight\":400,\"FontFamily\":\"微软雅黑\"},{\"ItemId\":\"QRCode-9699\",\"ItemName\":\"QRCode\",\"Code\":\"QRCode\",\"ItemType\":\"QRCode\",\"Width\":98,\"Height\":98,\"Left\":14,\"Top\":465,\"FontSize\":12,\"Weight\":400,\"FontFamily\":\"\'Helvetica Neue\', Helvetica, Arial, sans-serif\",\"Alignment\":\"left\"},{\"ItemId\":\"Code39-5329\",\"ItemName\":\"Code39\",\"Code\":\"Code39\",\"ItemType\":\"Code39\",\"Width\":319,\"Height\":68,\"Left\":122,\"Top\":466,\"FontSize\":12,\"Weight\":400,\"FontFamily\":\"\'Helvetica Neue\', Helvetica, Arial, sans-serif\",\"Alignment\":\"left\"},{\"ItemId\":\"Code93-1429\",\"ItemName\":\"Code93\",\"Code\":\"Code93\",\"ItemType\":\"Code93\",\"Width\":268,\"Height\":85,\"Left\":474,\"Top\":288,\"FontSize\":12,\"Weight\":400,\"FontFamily\":\"\'Helvetica Neue\', Helvetica, Arial, sans-serif\",\"Alignment\":\"left\"},{\"ItemId\":\"128Auto-205\",\"ItemName\":\"128Auto\",\"Code\":\"128Auto\",\"ItemType\":\"128Auto\",\"Width\":264,\"Height\":98,\"Left\":470,\"Top\":419,\"FontSize\":12,\"Weight\":400,\"FontFamily\":\"\'Helvetica Neue\', Helvetica, Arial, sans-serif\",\"Alignment\":\"left\"}],\"ItemDetailList\":{\"RowNumber-4151\":{\"ItemId\":\"RowNumber-4151\",\"ItemName\":\"行号\",\"Code\":\"RowNumber\",\"ItemType\":\"2\",\"Width\":55,\"Height\":26,\"Left\":18,\"Top\":151,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"center\"},\"goods-goods_name-1418-3337\":{\"ItemId\":\"goods-goods_name-1418-3337\",\"ItemName\":\"货品名称\",\"Code\":\"goods_name\",\"ItemType\":\"2\",\"Width\":186,\"Height\":27,\"Left\":79,\"Top\":151,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"center\"},\"goods-goods_weight-78784-4927\":{\"ItemId\":\"goods-goods_weight-78784-4927\",\"ItemName\":\"重量\",\"Code\":\"goods_weight\",\"ItemType\":\"2\",\"Width\":138,\"Height\":26,\"Left\":271,\"Top\":152,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"center\"},\"goods-goods_price-77199-4783\":{\"ItemId\":\"goods-goods_price-77199-4783\",\"ItemName\":\"售价\",\"Code\":\"goods_price\",\"ItemType\":\"2\",\"Width\":131,\"Height\":27,\"Left\":416,\"Top\":152,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"center\"},\"goods-goods_price-18470-2889\":{\"ItemId\":\"goods-goods_price-18470-2889\",\"ItemName\":\"售价\",\"Code\":\"goods_price\",\"ItemType\":\"2\",\"Width\":186,\"Height\":28,\"Left\":551,\"Top\":151,\"FontSize\":14,\"Weight\":400,\"FontFamily\":\"微软雅黑\",\"Alignment\":\"center\"},\"orderTitle\":{\"Top\":151,\"Left\":18,\"orderValue\":\"RowNumber-4151,goods-goods_name-1418-3337,goods-goods_weight-78784-4927,goods-goods_price-77199-4783,goods-goods_price-18470-2889\"}}}','on','on','400');
