<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
class PrintTemp extends Controller
{
    //打印标签页面
    public function index()
    {
        //var_dump(dirname(request()->domain().request()->root()));die();
        $fields = Db::table('print_fields')->where('parent_id',0)->select();
        return $this->fetch('index',['fields'=>$fields]);
    }

    //添加打印标签
    public function fieldAdd()
    {
        if($this->request->isPost()){
            $data = input('post.');
            if(empty($data['field_name'])) return json_encode(['success'=>false,'msg'=>'标识不可为空...']);
           
            if(empty($data['field_title'])) return json_encode(['success'=>false,'msg'=>'标题不可为空...']);
           
            if($data['parent_id'] != 0){
                $fieldRow = Db::table('print_fields')->field('print_type')->where('fieldid',$data['parent_id'])->find();
                $data['print_type'] = $fieldRow['print_type'];
            }else{
                if($data['print_type'] == 0) return json_encode(['success'=>false,'msg'=>'打印类型不可为空...']);
            }
          
            if(Db::table('print_fields')->insert($data)){
                return json_encode(['success'=>true,'msg'=>'添加成功...']);
            }else{
                return json_encode(['success'=>false,'msg'=>'添加失败...']);
            }
            
        }else{

            return $this->fetch('field_add');
        }
    }

    //查看下级打印标签
    public function fieldChild($pid)
    { 
        $fields = Db::table('print_fields')->where('parent_id',$pid)->order('sort asc')->select();
        return $this->fetch('field_child',['fields'=>$fields]);
    }

       
    //模板列表页面
    public function tempIndex()
    {
        $temp = Db::table('print_template')->select();
        return $this->fetch('temp_index',['temp'=>$temp]);
    }

    //添加打印模板
    public function tempAdd($id=0)
    {
        if($this->request->isPost()){
            $data = input('post.');
            if(empty($data['temp_name'])) return json_encode(['success'=>false,'msg'=>'模板名称不可为空...']);
            if(empty($data['temp_type'])) return json_encode(['success'=>false,'msg'=>'模板类型不可为空...']);
            if(empty($data['page_width'])) return json_encode(['success'=>false,'msg'=>'纸张宽度不可为空...']);
            if(empty($data['page_height'])) return json_encode(['success'=>false,'msg'=>'纸张高度不可为空...']);
            if(strlen($data['content']) < 36) return json_encode(['success'=>false,'msg'=>'打印窗体元素不可为空...']);
            if($id > 0){
                $res = Db::table('print_template')->update($data);
            }else{
                $res = Db::table('print_template')->insert($data);
            }
            if($res){
                return json_encode(['success'=>true,'msg'=>'操作成功...']);
            }else{
                return json_encode(['success'=>false,'msg'=>'操作失败...']);
            }
            
        }else{
           
            return $this->fetch('temp_add',['id'=>$id]);
        }
      
    }

    //获取打印模板
    public function getTempOne($id){
        $tempdata = json_encode(Db::table('print_template')->where('id',$id)->find());
        die($tempdata);
    }

    //根据打印模板类型获取右侧打印属性菜单
    public function getMenu()
    {
    	$mid = input('post.mid') ? input('post.mid') : 1;
        $menu = Db::query(
            "select *
            from print_mod_field as mf
            left join print_fields as f on mf.fid = f.fieldid or mf.fid = f.parent_id
            where mid=:mid order by f.fieldid asc",['mid'=>$mid]);
        $menu2 = $this->menuDeep($menu, 0);
        //var_dump(json_encode($menu2));die();
        return json_encode($menu2);
    }

    //整理menu树形结构
    public function menuDeep($menu, $level)
    {
    	$data =array();
    	foreach ($menu as  $value) {
    		if($value['parent_id'] == $level){
    		   $value['item_list'] = $this->menuDeep($menu, $value['fieldid']);
    		   $data[] = $value;
    		}
    	}
    	return $data;
    }

    //获取顶级打印标签
     public function getFieldList()
    {
        $fields = Db::table('print_fields')->where('parent_id',0)->select();
        return json_encode($fields);
    }

    //获取打印模块种类
    public function getPrintModule(){
        $module = Db::table('print_module')->select();
        return json_encode($module);
    }

    public function upload(){
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file(input('post.name'));
        // 移动到框架应用根目录/public/uploads/ 目录下
        if($file){
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
            if($info){
                $baseurl = request()->domain();
                $url = dirname(request()->domain().request()->root()). '/uploads/'.$info->getSaveName();
                $url=str_replace('\\','/',$url);
                die($url);
               
            }else{
                // 上传失败获取错误信息
                echo $file->getError();die();
              
            }
        }
    }

}
