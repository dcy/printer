var tempTypeList = {}; //模板类型

var innerTempData = {}; //内置模板
var courierNoList = {}; //物流公司
var courierProductTypeList = {}; //快递产品类型
var printerNameList = {}; //打印机列表

var oldTempData = {}; //还原模板所用
var stepArray = new Array(20); //撤销需要
var stepIndex = -1; //步骤
var revokeFlg = 1; //是否存储撤销步骤

var expressBkgrdList = {}; //参考背景类别
var category_no = ''; //左边的树形菜单id
var thisTempItemsData = {};

var dragresize;
var initPanel = 0;



function parseJson(data) {
	var obj;
	try {
		// data有可能不是合法的JSON字符串，便会产生异常
		obj = JSON.parse(data);
	} catch (e) {
		obj = {};
	}

	return obj;
}

/**
 * 将form 数据转换成json
 */
function toJson(formId) {
	var json = $(formId).serializeArray(); // 需要提交到后台的数据
	var result = {};
	/* 压缩默认的json数据，数据量更小 */
	for (var i = 0, l = json.length; i < l; i++) {
		//过滤多余的字段,包含ckbox的过滤掉
		if (json[i].name.indexOf("ckbox") > 0) {
			continue;
		}
		result[json[i].name] = json[i].value;
	}
	return result;
}

/*
 * 打印模板上的面板的相关操作
 
 * 面板界面初始化：全部设置为初始化值
 */
function pannelInit() {
	$(".drsElement").each(function() {
		this.remove();
	});
	if (initPanel == 0) {
		initPanel = 1;
	} else {
		//changedragresize();//纸张大小
		return;
	}
	$('.dropdown-toggle').dropdown(); //设置下拉菜单生效
	$.easing.def = "easeOutBounce";
	$('li.message_button a').click(function(e) {
		var dropDown = $(this).parent().next();
		$('.dropdown').not(dropDown).slideUp('slow');
		dropDown.slideToggle('slow');
		e.preventDefault();
	});

	initdragresize(); //拖动
	changedragresize(); //纸张大小
	$(".closed").removeClass("closed");

	setFontsize('10'); //字大小

};
/**
 * 清空面板
 */
function clearShow() {
	$(".drsElement").each(function() {
		this.remove();
	});
	$("#templaceMain").css("background-image", "");
	$("#printTemp #backgrd").val("");
}


/**
 * 输入框处理
 */
function initdragresize() {
	dragresize = new DragResize('dragresize', {
		minWidth: 8,
		minHeight: 8,
		minLeft: 0,
		minTop: 0,
		maxLeft: 858,
		maxTop: 1080
	});
	dragresize.isElement = function(elm) {
		if (elm.className && elm.className.indexOf('drsElement') > -1) return true;
	};
	dragresize.isHandle = function(elm) {
		if (elm.className && elm.className.indexOf('drsMoveHandle') > -1) return true;
	};
	dragresize.ondragfocus = function() {
		dragfocus();
	};
	dragresize.ondragstart = function(isResize) {
		if (isResize) {}
	};
	dragresize.ondragmove = function(isResize) {};
	dragresize.ondragend = function(isResize) {
		if (isResize) {}
	};
	dragresize.ondragblur = function() {
		dragblur();
	};

	dragresize.apply(document);
};
/**
 * 模板项焦点事件 
 */
function dragfocus() {
	$(".currentEl").css("background-color", ""); // 选中变色
	$(".currentEl").css("border-width", "1px"); // 选中变色
	$(".currentEl").css("opacity", ""); // 选中变色

	$("#txtContent").blur();
	$(".beniEl").removeClass("beniEl");
	$(".currentEl").removeClass("currentEl");
	$(".closed").removeClass("closed"); // Beni--鼠标失去焦点，去掉X号标签

	$(dragresize.element).addClass("currentEl");
	$(".currentEl a").addClass("closed"); // Beni--选中时，显示X号标签

	$(".currentEl").css("background-color", "#FFFF00"); // 选中变色
	$(".currentEl").css("border-width", "2px"); // 选中变色
	$(".currentEl").css("opacity", "0.85"); // 选中变色

	var name = $(".currentEl").attr("name");
	// Xu 变更横线
	if (name == "juxing") {
		$(".dragresize-tl").css("display", "none");
		$(".dragresize-tr").css("display", "none");
		$(".dragresize-bl").css("display", "none");
		$(".dragresize-br").css("display", "none");
		$(".dragresize-ml").css("display", "block");
		$(".dragresize-mr").css("display", "block");
		$(".dragresize-tm").css("display", "block");
		$(".dragresize-bm").css("display", "block");

		$(".currentEl").css("border-width", "1px"); // 选中变色
		$(".currentEl").css("opacity", "0.4"); // 选中变色
	} else if (name == "hengxian") {
		$(".dragresize-tl").css("display", "none");
		$(".dragresize-tm").css("display", "none");
		$(".dragresize-tr").css("display", "none");
		$(".dragresize-bl").css("display", "none");
		$(".dragresize-bm").css("display", "none");
		$(".dragresize-br").css("display", "none");
		$(".dragresize-ml").css("display", "block");
		$(".dragresize-mr").css("display", "block");
		$(".currentEl").css("opacity", "0.4"); // 选中变色
	} else if (name == "shuxian") {
		$(".dragresize-tl").css("display", "none");
		$(".dragresize-tr").css("display", "none");
		$(".dragresize-bl").css("display", "none");
		$(".dragresize-br").css("display", "none");
		$(".dragresize-ml").css("display", "none");
		$(".dragresize-mr").css("display", "none");
		$(".dragresize-tm").css("display", "block");
		$(".dragresize-bm").css("display", "block");
		$(".currentEl").css("opacity", "0.4"); // 选中变色
	} else if (name == "erweima") {
		$(".dragresize-tl").css("display", "block");
		$(".dragresize-tr").css("display", "block");
		$(".dragresize-bl").css("display", "block");
		$(".dragresize-br").css("display", "block");
		$(".dragresize-ml").css("display", "none");
		$(".dragresize-mr").css("display", "none");
		$(".dragresize-tm").css("display", "none");
		$(".dragresize-bm").css("display", "none");
	} else {
		$(".dragresize-tl").css("display", "block");
		$(".dragresize-tr").css("display", "block");
		$(".dragresize-bl").css("display", "block");
		$(".dragresize-br").css("display", "block");
		$(".dragresize-ml").css("display", "block");
		$(".dragresize-mr").css("display", "block");
		$(".dragresize-tm").css("display", "block");
		$(".dragresize-bm").css("display", "block");
	}

	// var fontsize = $(dragresize.element).children(".text").css("font-size");
	var fontsize = $(dragresize.element).children(".text").attr("fz");
	$("#ddlfontsize").val(fontsize);
	var fontweight = $(dragresize.element).children(".text").css("font-weight");
	if (fontweight == "bold" || fontweight == "700") {
		$("#ddlfontweight").val("700");
	} else {
		$("#ddlfontweight").val("400");
	}
	var fontfamliy = $(dragresize.element).children(".text").css("font-family");
	$("#moren_fontfamliy").val(fontfamliy);
	var txt = $(dragresize.element).text().split("^&");
	if (txt[1] == "custom_text") {
		$("#txtContent").val(txt[0]);
		// document.getElementById('txtContent').focus();
	} else {
		$("#txtContent").val("");
	}
	// 对齐方式
	var textAlign = $(dragresize.element).css("text-align");
	$("#textAlign").val(textAlign);
}
// 删除输入框
function closeDiv(id) {
	$("#" + id).remove();
	addRevokeArray();
}
// Beni--失去焦点事件
function dragblur() {
	var name = $(".currentEl").attr("name");
	if (name != "hengxian" && name != "shuxian" && name != "juxing") {
		if (name == "allnofhd") {
			$("#tb_order").width($("#templaceMain").width() - 20);
		}
		$(".currentEl").css("border", "1px dashed #FFB6C1");
	}
	addRevokeArray();
	// console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
}
//字体设置(全部输入框)
function FontSizeSetting(obj) {
	var fontsize = $(obj).val();
	$(".drsElement").children(".text").css("font-size", fontsize + "px");
	$("#ddlfontsize").val(fontsize);
}
//字体设置(当前选中的输入框)
function ChangeFontSize(obj) {
	var fontsize = $(obj).val();
	$(".currentEl").children(".text").attr("fz", fontsize);
	$(".currentEl").children(".text").css("font-size", fontsize + "px");
	if (fontsize < 12) {
		var pr = fontsize / 12;
		$(".currentEl").children(".text").css("-webkit-transform", "scale(" + pr + ")");
	} else {
		$(".currentEl").children(".text").css("-webkit-transform", "scale(1)");
	}
	$(".beniEl").css("font-size", fontsize + "px");
	$(".beniEl").children(".tbr").css("font-size", fontsize + "px");
	addRevokeArray(); //撤销步骤  
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
}
//字体设置(当前选中的输入框)
function ChangeFontFamily(obj) {
	var fontfamily = $(obj).val();
	$(".currentEl").children(".text").css("font-family", fontfamily);
	$(".beniEl").css("font-family", fontfamily);
	$(".beniEl").children(".tbr").css("font-family", fontfamily);
	addRevokeArray(); //撤销步骤
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
}
/**
 * 改变字体粗细
 */
function ChangeFontWeight(obj) {
	var fontweight = $(obj).val();
	$(".currentEl").children(".text").css("font-weight", fontweight);
	$(".beniEl").css("font-weight", fontweight);
	$(".beniEl").children(".tbr").css("font-weight", fontweight);
	addRevokeArray(); //撤销步骤
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
}
/**
 * 对齐方式text-align:center;
 * @param obj
 */
function ChangeTextAlign(obj) {
	var alignVar = $(obj).val();
	$(".currentEl").children(".text").css("text-align", alignVar);
	$(".currentEl").css("text-align", alignVar);
	addRevokeArray(); //撤销步骤
}

//元素移动
function wfElMoveTo(direction, value) //direction:top、left,value:1、-1
{
	$(".currentEl").each(function() {
		$(this).css(direction, parseInt($(this).css(direction).replace("px", "")) + value + "px");
	});
	addRevokeArray(); //撤销步骤 
}
/**
 * 移动 上
 * @param obj
 */
function ChangeShang(obj) {
	$(".currentEl").each(function() {
		$(this).css("top", parseInt($(this).css("top").replace("px", "")) - 1 + "px");
	});
	addRevokeArray(); //撤销步骤 
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
}
/**
 * 移动 下
 * @param obj
 */
function ChangeXia(obj) {
	$(".currentEl").each(function() {
		$(this).css("top", parseInt($(this).css("top").replace("px", "")) + 1 + "px");
	});
	addRevokeArray(); //撤销步骤  
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
}
/**
 * 移动 右
 * @param obj
 */
function ChangeRight(obj) {
	$(".currentEl").each(function() {
		$(this).css("left", parseInt($(this).css("left").replace("px", "")) + 1 + "px");
	});
	addRevokeArray(); //撤销步骤 
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
}
/**
 * 移动 左
 * @param obj
 */
function ChangeLeft(obj) {
	$(".currentEl").each(function() {
		$(this).css("left", parseInt($(this).css("left").replace("px", "")) - 1 + "px");
	});
	addRevokeArray(); //撤销步骤
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
}



//更换背景
function changeBg(url) {
	var bgWidth = $('#pageWidth').val(),
		bgHeight = $('#pageHeight').val();
	wfBackgroundChange(url, bgWidth, bgHeight);
}

function wfBackgroundChange(url, bgWidth, bgHeight) //url:背景地址,bgHeight:背景高度
{
	$("#templaceMain").css("background-image", "url(" + url + ")");

	if (bgWidth == '' || bgWidth == undefined || bgWidth == "0") {
		bgWidth = 'auto';
	} else {
		bgWidth = bgWidth + 'mm';
	}

	if (bgHeight == '' || bgHeight == undefined || bgHeight == "0") {
		bgHeight = 'auto';
	} else {
		bgHeight = bgHeight + 'mm';
	}
	$("#templaceMain").css("background-size", bgWidth + " " + bgHeight);
	addRevokeArray(); //撤销步骤
}
/**
 * 修改背景
 * @param obj
 */
function changeBkgrd(obj) {
	var expressBkgrd = $(obj).val();
	$("#backgrd").val(expressBkgrd);
	$("#expressBkgrd").val(expressBkgrd);
	$("#templaceMain").css("background-image", "url(" + expressBkgrd + ")");

	changeBkimgSize();
	//addRevokeArray();//撤销步骤 ---changeBkimgSize里面有加
}
///清除背景图片
function ClearBackground() {
	$("#templaceMain").css("background-image", "");
}
/**
 * 清理背景
 */
function clearBkg() {
	$("#templaceMain").css("background-image", "");
	$("#backgrd").val("");
	$("#bkimgHeight").val("");
	$("#bkimgWidth").val("");
	$("#expressBkgrd").val("-1");

}
/**
 * 修改背景大小
 * @param obj
 */
function changeBkimgSize() {
	var width = $("#bkimgWidth").val();
	if (width == '' || width == undefined) {
		width = 'auto';
	} else {
		width = width + 'mm';
	}
	$("#bkimgHeight").val(intCheck($("#bkimgHeight").val()));
	var height = $("#bkimgHeight").val();
	if (height == '' || height == undefined) {
		height = 'auto';
	} else {
		height = height + 'mm';
	}
	$("#templaceMain").css("background-size", width + " " + height);
	addRevokeArray(); //撤销步骤
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
}

/**
 * 上传文件
 * @param fileElementId
 * @param moduleType所在模块
 * @param title标题
 * @param backfileUrlid返回的url存放的id
 */
function fileUpload(fileElementId, callback) {
	$.ajaxFileUpload({
		url: uploadUrl,
		type: 'post',
		secureuri: false, //一般设置为false
		async: false,
		fileElementId: fileElementId, // 上传文件的id、name属性名
		dataType: 'text', //返回值类型，一般设置为json、application/json
		data: {
			name: fileElementId
		},
		complete: function() { //只要完成即执行，最后执行
		},
		success: function(data, status) {
			callback(data);
		},
		error: function(data, status, e) // 服务器响应失败处理函数
			{
				alert(data);
			}
	});
	// return false;
}
/**
 * 上传图片
 */
function uploadImage() {

	fileUpload('ImportFile', uploadImageBack);

}

function uploadImageBack(url) {
	$("#templaceMain").css('background-image', 'url("' + url + '")');
	$('#backgrd').val(url);
}
/**
 * 自定义图片上传
 */
function uploadImage2() {
	fileUpload('customImageFile', uploadCustomImageBack);
}

function uploadCustomImageBack(url) {
	try {
		AddSpecial("CustomImage", url);
	} catch (e) {}
}

function currentElSttting() {
	var currentId = $(".currentEl").attr("id");
	if (!(currentId == null || currentId == undefined)) {

		if (currentId.indexOf('kh_') == -1) {
			$(".currentEl").removeClass("currentEl");
		}
	}
}
//改变纸张大小
function changedragresize() {
	var bl = 3.774;

	var bt = 3.772;
	var obj_width = intCheck($("#printTemp #pageWidth").val());

	if (obj_width == "") {
		obj_width = $("#printTemp #pageWidth")[0].defaultValue;
	}
	$("#printTemp #pageWidth").val(obj_width);

	var obj_height = intCheck($("#printTemp #pageHeight").val());
	if (obj_height == "") {
		obj_height = $("#printTemp #pageHeight")[0].defaultValue;
	}
	$("#printTemp #pageHeight").val(obj_height);
	var width = parseFloat(obj_width) * bl;
	//////alert(obj_width);
	var height = parseFloat(obj_height) * bt;
	dragresize.maxTop = height; //height-15;//高度流出15像素
	dragresize.maxLeft = width;

	$("#templaceMain").css({
		"height": obj_height + "mm"
	});
	$("#templaceMain").css({
		"width": obj_width + "mm"
	});

	$("#moban_main").css({
		"width": (eval(obj_width) + 15) + "mm"
	});
	$("#moban_main").css({
		"height": (eval(obj_height) + 15) + "mm"
	});

	$("#rule").css({
		"width": (eval(obj_width) + 15) + "mm"
	});
	$("#rule").css({
		"height": (eval(obj_height) + 15) + "mm"
	});

	$("#printTemp #pageSizeMode").val("");
	addRevokeArray(); //撤销步骤
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));

}
//偏移
function changedMove(obj) {
	$(obj).val(intCheck($(obj).val()));
	if ($(obj).val() == "") {
		$(obj).val($(obj)[0].defaultValue);
	}
}



//弹出输入框
function wfInputDialogShow(type) {
	$("#mymodal").modal("toggle");
	$("#modalvalue").attr('attrid', type);
}

//输入框确认
function modalSubmit() {
	var inputValue = $("#modalvalue").val();
	var type = $("#modalvalue").attr('attrid'),
		id = type + '-' + parseInt(Math.random() * 10000);
	$("#modalvalue").val('');
	$("#modalvalue").attr('attrid', '');
	if (inputValue != '') {

		var width = 100;
		var height = 25;
		var left = 20;
		var top = 20;


		var fontsize = $('#ddlfontsize').val();
		var weight = $('#ddlfontweight').val();
		var family = $("#moren_fontfamliy").val();

		var textAlign = $("#textAlign").val();



		if (type == "QRCode" || type == "Code39" || type == "Code93" || type == "128Auto") {
			height = 100;
			$('.QRCodeBTN').hide()
		}
		AddElementToDrawBoard(id, type, width, height, left, top, fontsize, weight, family, textAlign, inputValue);
	}
	$('#mymodal').modal('hide');
}

//添加特殊元素：横线、竖线...,v:只有添加图片的时候用，图片路径
function AddSpecial(type, imageUrl) {
	var elementId = type + '-' + parseInt(Math.random() * 10000),
		width = "100",
		height = "25",
		left = "20",
		top = "20",
		fontSize = $('#ddlfontsize').val(),
		fontWeight = $('#ddlfontweight').val(),
		font = $("#moren_fontfamliy").val(),
		alignment = $("#textAlign").val();
	if (type == "CustomImage") {
		width = 150;
		height = 150;
	}
	AddElementToDrawBoard(elementId, type, width, height, left, top, fontSize, fontWeight, font, alignment, '', imageUrl)
}
//向模板添加元素
function AddElementToDrawBoard(id, txt, /*txttype,*/ width, height, left, top, fontsize, weight, family, textAlign, showText, imageUrl) {
	var el = "";
	if (txt == "SelfText") {
		//自定义文本 
		el = '<div id="' + id + '" txttype="' + txt + '" fieldCode="' + txt + '" class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';">' + showText + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txt == "HLine") {
		el = '<div id="' + id + '" name="hengxian" txttype="' + txt + '" fieldCode="' + txt + '" class="drsElement drsMoveHandle currentEl" style="left: ' + left + 'px; top: ' + top + 'px;height:8px; width:200px; border:1px dashed rgb(144, 221, 136);" ><div class="text" style="margin-top: 3px;border-top:1px solid #000000;"></div><a class="closed" style="top: -10px;" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txt == "VLine") {
		el = '<div id="' + id + '" name="shuxian" txttype="' + txt + '" fieldCode="' + txt + '" class="drsElement drsMoveHandle currentEl" style="left: ' + left + 'px; top: ' + top + 'px;height:200px; width:8px; border:1px dashed rgb(144, 221, 136);" ><div class="text" style="margin-left: 3px;border-left:1px solid #000000;"></div><a class="closed" style="left: 10px;" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txt == "Rect") {
		el = '<div id="' + id + '" name="juxing" txttype="' + txt + '" fieldCode="' + txt + '" class="drsElement drsMoveHandle currentEl" style="left: ' + left + 'px; top: ' + top + 'px;height:100px; width:100px; border:0px; border:1px solid #000000;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';"></div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	}
	// else if (txt == "Ellipse") {
	// 	el = '<div id="' + id + '" name="Ellipse" txttype="' + txt + '" fieldCode="' + txt + '" class="drsElement drsMoveHandle currentEl" style="left: ' + left + 'px; top: ' + top + 'px;height:100px; width:100px; border:0px; border:1px solid #000000;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';"></div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	// }
	 else if (txt == "PrintTime") {
		var t = "$[打印时间]"
		el = '<div id="' + id + '" txttype="' + txt + '" fieldCode="' + txt + '"  class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';">' + t + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txt == "QRCode") {
		el = '<div id="' + id + '" txttype="' + txt + '" fieldCode="' + txt + '" class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;background-image:url(\'' + baseUrl + '/static/lodop/img/' + id.split("-")[0] + '.png\');background-size:auto 100%;background-repeat: no-repeat no-repeat;opacity:0.7;filter:alpha(opacity=70);" ><div class="text" ></div><div style="border: 1px dashed #FFB6C1;" >' + txt + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txt == "Code39" || txt == "Code93" || txt == "128Auto") {
		el = '<div id="' + id + '" txttype="' + txt + '" fieldCode="' + txt + '" class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;background-image:url(\'' + baseUrl + '/static/lodop/img/' + id.split("-")[0] + '.png\');background-size:100% 100%;background-repeat: no-repeat no-repeat;opacity:0.7;filter:alpha(opacity=70);" ><div class="text" ></div><div style="border: 1px dashed #FFB6C1;" >' + txt + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txt == "CustomImage") {
		el = '<div id="' + id + '" txttype="' + txt + '" fieldCode="' + txt + '" txt="' + txt + '"  class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><img src="' + imageUrl + '" style="height:100%;width:100%"/><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txt == "RowNumber") {
		el = '<div id="' + id + '" txttype="' + 2 + '" fieldCode="' + txt + '"  class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:100px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';">行号</div><div style="border: 1px dashed #FFB6C1;" >[$data]</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	}


	$("#templaceMain").append(el);
	addRevokeArray(); //撤销步骤

}
/**
 * 增加模板展示项
 * 
 * @param id
 */
function appendMyText(idParam, txtParam) {

	var txt = $("#" + idParam).attr("value");

	var txttype = $("#" + idParam).attr("txttype");
	var id = idParam + "-" + parseInt(Math.random() * 10000);
	var width = 100;
	var height = 25;
	var left = 20;
	var top = 20;
	var fontsize = $('#ddlfontsize').val();
	var weight = $('#ddlfontweight').val();
	var family = $("#moren_fontfamliy").val();

	var textAlign = $("#textAlign").val();
	var fieldCode = $("#" + idParam).attr("fieldCode");

	setMyText(id, txt, txttype, width, height, left, top, fontsize, weight, family, textAlign, fieldCode);
};

/**
 * 增加模板展示项
 * @param id
 */
function setMyText(id, txt, txttype, width, height, left, top, fontsize, weight, family, textAlign, code) {
	//1普通文本数据   2表格数据
	var el = "";
	if (txttype == "1") {
		txt = "[$" + txt + "]";
		el = '<div id="' + id + '" txttype="' + txttype + '" fieldCode="' + code + '"  class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';">' + txt + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txttype == "2") {
		el = '<div id="' + id + '" txttype="' + txttype + '" fieldCode="' + code + '"  class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:100px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';">' + txt + '</div><div style="border: 1px dashed #FFB6C1;" >[$data]</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	}


	$("#templaceMain").append(el);
	addRevokeArray(); //撤销步骤

}
//填充模板
function fillPrintTemplate(tempData) {
	//console.log(tempData)
	$('#tempName').val(tempData.temp_name);
	if (tempData.temp_type != '' && tempData.temp_type != null) {
		$('#printTempMode').val(tempData.temp_type);
	} else {
		$('#printTempMode').val('0');
	}
	$('#intOrient').val(tempData.direction);
	if (tempData.printer_name != null && tempData.printer_name != '') {

		var sel = document.getElementById('printerName');
		for (var i = 0; i < sel.length; i++) {
			if (sel[i].text == tempData.printer_name) {
				sel[i].selected = true; //选中这一项
			}
		};
	}

	if (tempData.move_top > 0)
		$('#moveTop').val(tempData.move_top);
	if (tempData.move_left > 0)
		$('#moveLeft').val(tempData.move_left);
	if (tempData.page_width > 0)
		$('#pageWidth').val(tempData.page_width);
	if (tempData.page_height > 0)
		$('#pageHeight').val(tempData.page_height);
	$('#backgrd').val(tempData.background);
	if (tempData.bkimg_width > 0)
		$('#bkimgWidth').val(tempData.bkimg_width);
	if (tempData.bkimg_height > 0)
		$('#bkimgHeight').val(tempData.bkimg_height);
	$('#moren_fontfamliy').val(tempData.default_font_family);
	if (tempData.default_font_size > 0)
		$('#ddlfontsize').val(tempData.default_font_size);
	if (tempData.font_weight > 0)
		$('#ddlfontweight').val(tempData.font_weight);
	$('#textAlign').val(tempData.text_align);

	$('#bkimgFlg').prop('checked', tempData.bkimg_flg == 'on');
	$('#TableHasBorder').prop('checked', tempData.table_has_border == 'on');
	$('#TableHasHeader').prop('checked', tempData.table_has_header == 'on');

	var content = tempData.content;

	if (content != null && content != '') {
		content = eval('(' + content + ')');
	}

	if (content.ItemList != null && content.ItemList != '') {
		tempData.ItemList = content.ItemList;
	}
	if (content.ItemDetailList != null && content.ItemDetailList != '') {
		tempData.ItemDetailList = content.ItemDetailList;
	}
	if (tempData.ItemList != null) {

		$.each(tempData.ItemList, function(key, data) {
			editSetTempText(data.ItemId, data.ItemName, data.ItemType, parseInt(data.Width)+2 , parseInt(data.Height)+2 , data.Left, data.Top, data.FontSize, data.Weight, data.FontFamily, data.Alignment, data.Code);
		});
	}
	if (tempData.ItemDetailList != null) {
		var td = tempData.ItemDetailList;
		$.each(td, function(key, data) {
			editSetTempText(data.ItemId, data.ItemName, data.ItemType, parseInt(data.Width)+2 , parseInt(data.Height)+2, data.Left, data.Top, data.FontSize, data.Weight, data.FontFamily, data.Alignment, data.Code);
		});
	}
	if (tempData.background != null && tempData.background != '')
		$("#templaceMain").css("background-image", "url(" + tempData.background + ")"); //背景

	wfSetPageSize(tempData.page_width, tempData.page_height); //画布大小
	changeBkimgSize()
}
/**
 * 增加模板展示项
 * @param id
 */
function editSetTempText(id, txt, txttype, width, height, left, top, fontsize, weight, family, textAlign, code) {

	var el = "";

	if (txttype == "SelfText") {
		el = '<div id="' + id + '" txttype="' + txttype + '" fieldCode="' + code + '" class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';">' + txt + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	}else  if(txttype == "HLine") {
    	//width = parseInt(width)-2;
        el = '<div id="' + id + '" name="hengxian" txttype="'+txttype+'" fieldCode="'+code+'" class="drsElement drsMoveHandle currentEl" style="left: ' + left + 'px; top: ' + top + 'px;height:8px; width:' + width + 'px; border:1px dashed rgb(144, 221, 136);" ><div class="text" style="margin-top: 3px;border-top:1px solid #000000;"></div><a class="closed" style="top: -10px;" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
    }else if(txttype == "VLine") {
    	//height = parseInt(height)-2;
        el = '<div id="' + id + '" name="shuxian" txttype="'+txttype+'" fieldCode="'+code+'" class="drsElement drsMoveHandle currentEl" style="left: ' + left + 'px; top: ' + top + 'px;height:' + height + 'px;  width:8px;border:1px dashed rgb(144, 221, 136);" ><div class="text" style="margin-left: 3px;border-left:1px solid #000000;"></div><a class="closed" style="left: 10px;" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
    }else if(txttype == "Rect") {
        el = '<div id="' + id + '" name="juxing" txttype="'+txttype+'" fieldCode="'+code+'" class="drsElement drsMoveHandle currentEl" style="left: ' + left + 'px; top: ' + top + 'px;height:' + height + 'px; width:' + width + 'px; border:0px; border:1px solid #000000;" ><div class="text" fz="'+fontsize+'" style="font-size: '+fontsize+'px;font-weight:' + weight + ';font-family: ' + family + ';"></div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
    } else if (txttype == "PrintTime") {
		el = '<div id="' + id + '" txttype="' + txttype + '" fieldCode="' + code + '" class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';">' + txt + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txttype == "QRCode") {
		el = '<div id="' + id + '" txttype="' + txttype + '" fieldCode="' + txt + '" class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;background-image:url(\'' + baseUrl + '/static/lodop/img/' + id.split("-")[0] + '.png\');background-size:auto 100%;background-repeat: no-repeat no-repeat;opacity:0.7;filter:alpha(opacity=70);" ><div class="text" ></div><div style="border: 1px dashed #FFB6C1;" >' + txt + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txttype == "Code39" || txttype == "Code93" || txttype == "128Auto") {
		el = '<div id="' + id + '" txttype="' + txttype + '" fieldCode="' + txt + '" class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;background-image:url(\'' + baseUrl + '/static/lodop/img/' + id.split("-")[0] + '.png\');background-size:100% 100%;background-repeat: no-repeat no-repeat;opacity:0.7;filter:alpha(opacity=70);" ><div class="text" ></div><div style="border: 1px dashed #FFB6C1;" >' + txt + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txttype == "CustomImage") {
		el = '<div id="' + id + '" txttype="' + txttype + '" fieldCode="' + txttype + '" txt="' + txt + '"  class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><img src="' + txt + '" style="height:100%;width:100%"/><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txttype == "1") {
		el = '<div id="' + id + '" txttype="' + txttype + '" fieldCode="' + code + '" class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';">' + txt + '</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	} else if (txttype == "2") {
		el = '<div id="' + id + '" txttype="' + txttype + '" fieldCode="' + code + '"  class="drsElement drsMoveHandle currentEl" style="text-align:' + textAlign + ';left: ' + left + 'px; top: ' + top + 'px;width:' + width + 'px;height:' + height + 'px; border:1px dashed #FFB6C1;" ><div class="text" fz="' + fontsize + '" style="font-size: ' + fontsize + 'px;font-weight:' + weight + ';font-family: ' + family + ';">' + txt + '</div><div style="border: 1px dashed #FFB6C1;" >[$data]</div><a class="closed" onclick="closeDiv(\'' + id + '\');" title="删除"></a></div>';
	}

	$("#templaceMain").append(el);
};



/**
 * 将模板的菜单项目设置展示出来-----------------------------------
 * @param tempItemsData
 */
function setItems(tempItemsData) {

	var panel = ''
	$.each(tempItemsData, function(n, item) {
		//console.log(item)
		var collectionTitle = item.field_title;
		var collectionName = item.field_name;
		var dataType = item.print_type;
		var rodm = parseInt(Math.random() * 100000);
		var id = collectionName + '-' + rodm;
		var expanded = n == 0 ? 'true' : 'false';
		panel += '<div class="panel panel-default">'
		panel += '<div class="panel-heading" role="tab" id="' + id + '">'
		panel += '<h4 class="panel-title">'
		panel += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#' + collectionName + '" aria-expanded="false" aria-controls="' + collectionName + '">'
		panel += collectionTitle
		panel += '</a>'
		panel += '</h4>'
		panel += '</div>'
		panel += '<div id="' + collectionName + '" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="' + id + '">'
		panel += '<div class="panel-body">'
		panel += '<ul class="list-group">'

		if (item.item_list) {
			$.each(item.item_list, function(k, v) {
				//console.log(v)
				var collectionTitle2 = v.field_title;
				var collectionName2 = v.field_name;
				var dataType2 = v.print_type;
				var rodm2 = parseInt(Math.random() * 100000);
				var id2 = collectionName + '-' + collectionName2 + '-' + rodm2;

				//panel += '<li class="list-group-item"><a onclick="appendMyText(\'' + id2 + '\');" fieldCode="' + collectionName2 + '"   value="' + collectionTitle2 + '" id="' + id2 + '" txttype="' + dataType2 + '"><img src="'+baseUrl+'/static/lodop/img/add.png" style="width: 14px; height: 14px;">&nbsp;' + collectionTitle2 + ' </a></li>';
				panel += '<li class="list-group-item"><a onclick="appendMyText(\'' + id2 + '\');" fieldCode="' + collectionName2 + '"   value="' + collectionTitle2 + '" id="' + id2 + '" txttype="' + dataType2 + '"><span class="glyphicon glyphicon-plus"> </span>&nbsp;' + collectionTitle2 + ' </a></li>';

			})
		}
		panel += '</ul>'
		panel += '</div>'
		panel += '</div>'
		panel += '</div>'
	})

	$("#accordion").html(panel);

}



/**
 * 组装界面数据
 * 
 * @returns {___anonymous1572_1579}
 */
function getPageData() {
	// 输入框列表
	var itemList = [];
	var itemDetailList = [];
	$(".drsElement").each(function() {

		var size = "0";
		var weight = 400;
		size = $(this).children(".text").attr("fz");
		if (typeof(size) == "undefined") {
			size = 12;
		}

		if ($(this).children(".text").css("font-weight") == 'bold' || $(this).children(".text").css("font-weight") > 400) {
			weight = 700;
		} else {
			weight = 400;
		}

		var item = {};
		item.ItemId = $(this).attr("id");
		item.ItemName = $(this).text();
		item.Code = $(this).attr("fieldCode");
		item.ItemType = $(this).attr("txttype");
		if (item.ItemType == '2') {
			var w=item.ItemName.indexOf('[');
			item.ItemName = item.ItemName.substring(0,w);
		}
		item.Width = parseInt($(this).width());
		item.Height = parseInt($(this).height());
		// alert(item.height);
		var pos = $(this).position();
		item.Left = parseInt(pos.left);
		item.Top = parseInt(pos.top);

		item.FontSize = parseInt(size);
		item.Weight = parseInt(weight);
		item.FontFamily = $(this).children(".text").css("font-family");
		if (item.FontFamily == '' || item.FontFamily == undefined || item.FontFamily == 'undefined') {
			item.FontFamily = '微软雅黑';
		}
		var alignment = $(this).children(".text").css("text-align");

		item.Alignment = alignment;

		if (item.ItemType == 'CustomImage') {
			item.ItemName = $(this).children('img').attr("src");
			
		}
		if (item.ItemType == '2') {
			itemDetailList.push(item);
		} else {
			itemList.push(item);
		}
	});

	var postdata = toJson("#tempForm"); // form 要有name

	postdata.ItemList = itemList;
	postdata.ItemDetailList = dealDetailList(itemDetailList);

	return postdata;
};
/**
 * 处理明细数据排序
 */
function dealDetailList(itemDetailList) {
	var length = itemDetailList.length;
	if (length == 0) {
		return {};
	}
	var returnList = {};
	var top = itemDetailList[0].Top;
	var left = 0;
	var orderValue = '';

	if (length == 1) {
		orderValue = itemDetailList[0].ItemId;
	} else {
		for (var out = 0; out < length - 1; out++) {
			var orderItem = itemDetailList[out];
			for (var i = out + 1; i < length; i++) {
				if (itemDetailList[i].Left < orderItem.Left) {
					orderItem = itemDetailList[i];
					itemDetailList[i] = itemDetailList[out];
					itemDetailList[out] = orderItem;
				}
			}
			if (top > itemDetailList[out + 1].Top) {
				top = itemDetailList[out + 1].Top;
			}
			if (orderValue == '') {
				orderValue = orderItem.ItemId;
				left = orderItem.Left;
			} else {
				orderValue = orderValue + "," + orderItem.ItemId;
			}
			returnList[itemDetailList[out].ItemId] = itemDetailList[out];
		}
		orderValue = orderValue + "," + itemDetailList[length - 1].ItemId;
	}
	returnList[itemDetailList[length - 1].ItemId] = itemDetailList[length - 1]; //少加了最后一行，添加上

	var orderTitle = {};
	orderTitle.Top = top;
	orderTitle.Left = left;
	orderTitle.orderValue = orderValue;
	returnList.orderTitle = orderTitle;
	//console.log(JSON.stringify(returnList));	
	return returnList;
};

/**
 * 编辑时候预览
 */
function editPreviewShow() {
	var postdata = getPageData();
	var LODOP = lodopInitPage(postdata, getTestData());
	if (LODOP == null) {
		return;
	}
	LODOP.SET_SHOW_MODE("NP_NO_RESULT", true); // 设置NP插件无返回，这可避免chrome（谷歌）对弹窗超时误报崩溃。
	LODOP.PREVIEW();
};



/**
 * 更换内容模板
 */
// function changeTempMode() {
// 	revokeFlg = 0;
// 	var code = $("#printTemp #printTempMode").val();
// 	if (code != "-1") {
// 		////alert(JSON.stringify(innerTempData["type"+$("#printTemp #tempType").val()][code]));
// 		//showPrintTempInit();
// 		var id = $("#printTemp #id").val();
// 		var mysqlid = $("#printTemp #mysqlid").val();
// 		var mysqlno = $("#printTemp #mysqlno").val();
// 		$(".drsElement").each(function() {
// 			this.remove();
// 		});
// 		//editTempLoadData(innerTempData["type"+$("#printTemp #tempType").val()][code]);
// 		$("#printTemp #id").val(id);
// 		$("#printTemp #mysqlid").val(mysqlid);
// 		$('#printTemp #mysqlno').val(mysqlno);
// 	}
// 	revokeFlg = 1;
// 	addRevokeArray();
// 	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
// }
/**
 * 改变打印纸张大小
 * @param obj
 */
function changePageSizeMode(obj) {
	var selectSize = $(obj).val();
	if (selectSize != "") {
		$("#printTemp #pageWidth").val(selectSize.split("-")[0]);
		$("#printTemp #pageHeight").val(selectSize.split("-")[1]);
		changedragresize();
	}
}
//改变模板大小
function wfSetPageSize(obj_width, obj_height) //obj_width:宽度,obj_height:高度
{
	var bl = 3.774;

	var bt = 3.772;
	var width = parseFloat(obj_width) * bl;
	//////alert(obj_width);
	var height = parseFloat(obj_height) * bt;

	dragresize.maxTop = height; //height-15;//高度流出15像素
	dragresize.maxLeft = width;

	$("#templaceMain").css({
		"height": obj_height + "mm"
	});
	$("#templaceMain").css({
		"width": obj_width + "mm"
	});

	$("#moban_main").css({
		"width": (parseFloat(obj_width) + 15) + "mm"
	});
	$("#moban_main").css({
		"height": (parseFloat(obj_height) + 15) + "mm"
	});

	$("#rule").css({
		"width": (parseFloat(obj_width) + 15) + "mm"
	});
	$("#rule").css({
		"height": (parseFloat(obj_height) + 15) + "mm"
	});

	addRevokeArray(); //撤销步骤
}
/**
 * 获取相关数据，并放到obj中
 */
// function setListValue(url, obj, idField, nameField) {
// 	if (!$.isEmptyObject(obj)) {
// 		return;
// 	}
// 	$.ajax({
// 		type: 'get',
// 		url: url,
// 		async: false,
// 		success: function(items) {
// 			_.each(items, function(item) {
// 				obj[item[idField]] = item[nameField];
// 			});
// 			//console.log(JSON.stringify(obj));
// 		}
// 	});
// };



/**
 * 字体大小
 */
function setFontsize(fontSize) {
	/*$("#ddlfontsize option[value!='']").each(function() {
		this.remove();
	});
	for (var i = 8; i < 50; i++) {
		$("#ddlfontsize").append("<option value=" + i + ">" + i + "</option>");
		i++
	}*/

	if (fontSize != "") {
		$("#ddlfontsize").val(fontSize);
	}
};
/**
 * 校验数字，替换掉非数据的值
 * @param txt
 * @returns
 */
function intCheck(txt) {
	if (!txt) return 0;
	var reg = /[^\-?\d+$]/g;
	return txt = txt.replace(reg, '');
}

/**
 * 返回时间2016-05-05 12:12:12
 * @returns {String}
 */
function JsonCurDate() {
	var d = new Date();
	var y = d.getFullYear();
	var m = d.getMonth() + 1;
	if (m.toString().length == 1) {
		m = "0" + m;
	}
	var _d = d.getDate();
	if (_d.toString().length == 1) {
		_d = "0" + _d;
	}
	var h = d.getHours();
	if (h.toString().length == 1) {
		h = "0" + h;
	}
	var _m = d.getMinutes();
	if (_m.toString().length == 1) {
		_m = "0" + _m;
	}
	var s = d.getSeconds();
	if (s.toString().length == 1) {
		s = "0" + s;
	}

	return y + '-' + m + '-' + _d + ' ' + h + ':' + _m + ':' + s;
}
/**
 * 还原
 */
function restoreShow() {
	clearShow();
	if (!$.isEmptyObject(oldTempData)) {
		editTempLoadData(oldTempData);
	}
	initRevoke();
}

/**
 * 撤销init
 */
function initRevoke() {
	stepArray = new Array(20);
	stepIndex = 0;
	stepArray[stepIndex] = getPageData();
}

/**
 * 撤销
 */
function revokeShow() {
	revokeFlg = 0;
	stepIndex = stepIndex - 1;
	if (stepIndex >= 0) {
		clearShow();
		//console.log(JSON.stringify(stepArray[stepIndex]));
		editTempLoadData(stepArray[stepIndex]);
	} else {
		stepIndex = 0;
	}
	//console.log("stepArray ="+stepIndex);//+"==="+JSON.stringify(stepArray));
	revokeFlg = 1;
}
/**
 * 存储撤销的步骤
 */
function addRevokeArray() {
	if (revokeFlg < 1) {
		return;
	}
	var arryLength = stepArray.length;
	stepIndex += 1;
	if (stepIndex >= arryLength) {
		for (var x = 0; x < arryLength - 1; x++) {
			stepArray[x] = stepArray[x + 1]
		}
		stepIndex = stepIndex - 1;
	}
	var postdata = getPageData();
	stepArray[stepIndex] = postdata;
	//console.log("addRevokeArray ="+stepIndex+",revokeFlg="+revokeFlg);//+"==="+JSON.stringify(stepArray));
}
/**
 * 退回已撤销步骤
 * @param stepSize
 */
function subRevokeArray(stepSize) {
	if (revokeFlg < 1) {
		return;
	}
	for (var x = 1; x <= stepSize; x++) {
		if (stepIndex > -1) {
			stepArray[stepIndex] = "";
			stepIndex = stepIndex - 1;
		}
	}
}
/**
 * 获取url后面制定参数
 * @param name
 * @returns
 */
function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) return (r[2]);
	return null;
}



if (document.addEventListener) {
	document.addEventListener("keyup", fnKeyup, true);
} else {
	document.attachEvent("onkeyup", fnKeyup);
}

function fnKeyup(event) {
	// 46 删除键删除
	if (event.keyCode == 46) {
		$(".currentEl").remove();
		addRevokeArray();
	}
}

$('.dropdown-menu-btn').click(function() {
	$('.dropdown-menu-list').toggle()
})


//获取打印机列表

function setPrinter(id, em) {
	setTimeout(function() {
		var lodop = getLodop(document.getElementById('LODOP_OB'), document.getElementById('LODOP_EM'));
		if (lodop == null) {
			return {};
		}

		lodop.Create_Printer_List(document.getElementById('printerName'));
	}, 1000)

}
//获取纸张尺寸列表
function getPageSizeList() {
	setTimeout(function() {
		var lodop = getLodop(document.getElementById('LODOP_OB'), document.getElementById('LODOP_EM'));
		if (lodop == null) {
			return {};
		}
		lodop.Create_PageSize_List(document.getElementById('pageSizeMode'), 2);
	}, 1000)
	
}